# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://davidmskc@bitbucket.org/davidmskc/stroboskop.git
git add .
```

Naloga 6.2.3:
https://bitbucket.org/davidmskc/stroboskop/commits/05aaa81e12d81a3dbd22645afed865765f78d147

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/davidmskc/stroboskop/commits/5295e8ef0b0b66f8f882417aada9d93f0e0ba657

Naloga 6.3.2:
https://bitbucket.org/davidmskc/stroboskop/commits/a4ee4d1071aed88569a4c6cfa2fa8986b55560cf

Naloga 6.3.3:
https://bitbucket.org/davidmskc/stroboskop/commits/b1b296bf9a8fbd5cd07dd07d17f24ad603415f17

Naloga 6.3.4:
https://bitbucket.org/davidmskc/stroboskop/commits/78a1b17f311259bcf7a0c5ad01afcb65f2926a11

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/davidmskc/stroboskop/commits/939583e50e48c35cf60b79dc00f1c3ef976d46e6

Naloga 6.4.2:
https://bitbucket.org/davidmskc/stroboskop/commits/5730950ce069269557eeb8a173c406617a33ff29

Naloga 6.4.3:
https://bitbucket.org/davidmskc/stroboskop/commits/2cadd40c10e4813ac7d808625bfb0713e82af5d7

Naloga 6.4.4:
https://bitbucket.org/davidmskc/stroboskop/commits/c3f9385888fec4fda78581e77137a42d3001c026